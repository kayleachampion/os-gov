Thank you for your interest in participating in the os-gov project! 

There are several ways to join us:

1 - As a writer: we need writers to articulate and synthesize recommended practices from across a broad spectrum of evidence: qualitative evidence from personal experience, descriptive evidence from expert observation of the open source movement as a whole, and quantitative evidence from scientific analysis. To join the project as a writer, sign up as an Eclipse committer, then fork our repository and submit pull requests for review and merger.

2 - By participating in deliberating and planning: we make decisions by consensus to the extent possible. We seek diverse points of view and to reflect a broad perspective while still offering specific and actionable advice; attendees at project meetings may call for a formal vote as they see fit.

3 - Offering commenting and feedback: Please feel free to use gitlab features to open issues or propose objections; alternately, consider joining our mailing list.

4 - Pitching in with organizing and marshalling: We meet approximately monthly via a video call as well as coordinating work through our mailing list. Anyone is welcome to join these open calls. 
