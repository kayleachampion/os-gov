# OS-Gov

The Eclipse OS-Gov project provides guidelines and best practices for the governance and management of open source projects. Developed by recognised open source experts, it covers a wide range of topics, from tooling to communication and security. It only provides recommendations, as projects should adapt their practices to their own context.

We intend to develop these recommendations in a collaborative manner with the community, and publish various outputs (PDF, epub, HTML) freely available to everybody.

## Contributing

We are open, and warmly welcome any contribution! 

* If you already contributed to the [February session in Brussels](https://pad.castalia.camp/mypads/?/mypads/group/ieee-osspg-jyi0pyt/view), please [fill in the survey](https://framaforms.org/osspg-next-steps-1693469325) to help us reuse it.
* You can subscribe to our [mailing list](https://accounts.eclipse.org/mailing-list/os-gov-dev), introduce yourself, and join the discussion!

Like for most open source projects, contributions happen by forks and merge requests.

Please feel free to submit issues, propose merge requests, or ask any question you may have! For more details, check our CONTRIBUTING.md file.

## Organisation

* Minutes are stored in the [project's wiki](https://gitlab.eclipse.org/groups/eclipse/os-gov/-/wikis/home).
* Our approach to governing this project can be found in GOVERNANCE.md.
